#ifndef ASS_H
#define	ASS_H

#include "uart/pic24_uart.h"

#define pic24_assert(expression)  \
 ((void)((expression) ? 0 : (__myassert ( __FILE__, __FUNCTION__, __LINE__), 0)))

#define __myassert( file, func, line)  \
 __myassfail("Fault assert in file %s at line %d function '%s'.\n\r",    \
        file, line, func)

static void __myassfail(const char *format,...)
{
   va_list arg;
   static char mystderr[255];
   char s[255];
   va_start(arg, format);
   (void)vsprintf(&mystderr[0], format, arg);
   memcpy(s,&mystderr[0],255);
   uart_print(s);
   
   LATBbits.LATB2 = 1;
           
   while(1);
   va_end(arg);
}

#endif